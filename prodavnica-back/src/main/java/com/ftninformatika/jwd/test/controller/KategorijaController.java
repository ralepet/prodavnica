package com.ftninformatika.jwd.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.KategorijaDto;
import com.ftninformatika.jwd.test.model.Kategorija;
import com.ftninformatika.jwd.test.service.KategorijaService;
import com.ftninformatika.jwd.test.support.KategorijaToKategorijaDto;

@RestController
@RequestMapping(value = "/api/kategorije", produces = MediaType.APPLICATION_JSON_VALUE)
public class KategorijaController {
	
	@Autowired
	private KategorijaService kategorijaService;
	
	@Autowired
	private KategorijaToKategorijaDto toDto;
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<KategorijaDto>> getAll() {

		List<Kategorija	> list = kategorijaService.findAll();

		return new ResponseEntity<>(toDto.convert(list), HttpStatus.OK);
	}

}
