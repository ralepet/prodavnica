package com.ftninformatika.jwd.test.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.PorudzbinaDto;
import com.ftninformatika.jwd.test.model.Porudzbina;
import com.ftninformatika.jwd.test.service.PorudzbinaService;
import com.ftninformatika.jwd.test.service.ProizvodService;
import com.ftninformatika.jwd.test.support.PorudzbinaDtoToPorudzbina;
import com.ftninformatika.jwd.test.support.PorudzbinaToPorudzbinaDto;



@RestController
@RequestMapping(value = "/api/porudzbine", produces = MediaType.APPLICATION_JSON_VALUE)
public class PorudzbinaController {
	
	@Autowired
	private PorudzbinaService porudzbinaService; 
	

	@Autowired
	private ProizvodService proizvodService; 
	
	@Autowired
	private PorudzbinaToPorudzbinaDto toDto;
	
	@Autowired
	private PorudzbinaDtoToPorudzbina toPorudzbina;
	
	@PreAuthorize("hasRole('KORISNIK')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PorudzbinaDto> kupi(@Valid @RequestBody PorudzbinaDto porduzbinaDto) { 
		
		Porudzbina porduzbina = toPorudzbina.convert(porduzbinaDto);
 
		if (porduzbina.getProizvod() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		if(porduzbinaDto.getKolicina() > porduzbina.getProizvod().getStanje()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		proizvodService.oduzmiStanje(porduzbina.getProizvod(), porduzbinaDto.getKolicina());

		Porudzbina sacuvanaPorudzbina = porudzbinaService.save(porduzbina);

		return new ResponseEntity<>(toDto.convert(sacuvanaPorudzbina), HttpStatus.CREATED);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(value = "/naruci",consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PorudzbinaDto> naruci(@Valid @RequestBody PorudzbinaDto porduzbinaDto) {
		Porudzbina porudzbina = toPorudzbina.convert(porduzbinaDto); 

		if (porudzbina.getProizvod() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST); 
		}
		
		proizvodService.dodajStanje(porudzbina.getProizvod(), porduzbinaDto.getKolicina());

		Porudzbina sacuvanPorudzbina = porudzbinaService.save(porudzbina);

		return new ResponseEntity<>(toDto.convert(sacuvanPorudzbina), HttpStatus.CREATED);
	}

	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

}
