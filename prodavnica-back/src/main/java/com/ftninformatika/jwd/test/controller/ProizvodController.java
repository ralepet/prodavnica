package com.ftninformatika.jwd.test.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.ProizvodDto;
import com.ftninformatika.jwd.test.model.Proizvod;
import com.ftninformatika.jwd.test.service.ProizvodService;
import com.ftninformatika.jwd.test.support.ProizvodDtoToProizvod;
import com.ftninformatika.jwd.test.support.ProizvodToProizvodDto;


@RestController
@RequestMapping(value = "/api/proizvodi", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProizvodController {
	

	@Autowired
	private ProizvodService proizvodService;

	@Autowired
	private ProizvodToProizvodDto toDto;
	
	@Autowired
	private ProizvodDtoToProizvod toProizvod;
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<ProizvodDto>> getAll(
			@RequestParam(required=false) Double cenaOd, 
			@RequestParam(required=false) Double cenaDo,
			@RequestParam(required=false) Long kategorija_id,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {

		Page<Proizvod> page = proizvodService.find(cenaOd, cenaDo, kategorija_id, pageNo);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(page.getContent()), headers, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<ProizvodDto> getOne(@PathVariable Long id) {
		Proizvod proizvod = proizvodService.findOneById(id);

		if (proizvod != null) {
			return new ResponseEntity<>(toDto.convert(proizvod), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProizvodDto> create(@Valid @RequestBody ProizvodDto proizvodDto) {
		Proizvod proizvod = toProizvod.convert(proizvodDto);

		System.out.println();
		System.out.println(proizvodDto);
		System.out.println();

		System.out.println();
		System.out.println(proizvod);
		System.out.println();

		Proizvod sacuvanProizvod = proizvodService.save(proizvod);

		return new ResponseEntity<>(toDto.convert(sacuvanProizvod), HttpStatus.CREATED);
	}

	@PreAuthorize("hasAnyAuthority('ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProizvodDto> update(@PathVariable Long id, @Valid @RequestBody ProizvodDto proizvodDto) {

		if (!id.equals(proizvodDto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Proizvod proizvod = toProizvod.convert(proizvodDto);
		Proizvod sacuvanProizvod = proizvodService.update(proizvod);

		return new ResponseEntity<>(toDto.convert(sacuvanProizvod), HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Proizvod obrisanProizvod = proizvodService.delete(id);

		if (obrisanProizvod != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

}
