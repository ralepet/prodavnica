package com.ftninformatika.jwd.test.dto;

public class PorudzbinaDto {
	
	private Long id;

	private Integer kolicina;
	
	private Long idProizvoda;
	
	private String nazivProizvoda;

	public PorudzbinaDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	public Long getIdProizvoda() {
		return idProizvoda;
	}

	public void setIdProizvoda(Long idProizvoda) {
		this.idProizvoda = idProizvoda;
	}

	public String getNazivProizvoda() {
		return nazivProizvoda;
	}

	public void setNazivProizvoda(String nazivProizvoda) {
		this.nazivProizvoda = nazivProizvoda;
	}
	
	

}
