package com.ftninformatika.jwd.test.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class ProizvodDto {
	
	private Long id;
	
	@Positive
	@NotNull
	private Double cena;

	@Positive
	@NotNull
	private Integer stanje;
	
	@NotBlank
	@Size(max = 15)
	private String naziv;
	
	private Long idKategorije;
	
	private String nazivKategorije;

	public ProizvodDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}


	public Integer getStanje() {
		return stanje;
	}

	public void setStanje(Integer stanje) {
		this.stanje = stanje;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Long getIdKategorije() {
		return idKategorije;
	}

	public void setIdKategorije(Long idKategorije) {
		this.idKategorije = idKategorije;
	}

	public String getNazivKategorije() {
		return nazivKategorije;
	}

	public void setNazivKategorije(String nazivKategorije) {
		this.nazivKategorije = nazivKategorije;
	}

	@Override
	public String toString() {
		return "ProizvodDto [id=" + id + ", cena=" + cena + ", stanje=" + stanje + ", naziv=" + naziv
				+ ", idKategorije=" + idKategorije + ", nazivKategorije=" + nazivKategorije + "]";
	}
	
	
	
}
