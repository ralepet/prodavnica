package com.ftninformatika.jwd.test.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.test.model.Proizvod;

@Repository
public interface ProizvodRepository extends JpaRepository<Proizvod, Long>{
	
	Proizvod findOneById(Long id);

	Page<Proizvod> findByCenaBetweenAndKategorijaId(Double cenaOd, Double cenaDo,Long kategorija_id, Pageable pageable);

	Page<Proizvod> findByCenaBetween(Double cenaOd, Double cenaDo, Pageable pageable);

}
