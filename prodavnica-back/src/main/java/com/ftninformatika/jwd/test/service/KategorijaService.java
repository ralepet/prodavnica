package com.ftninformatika.jwd.test.service;

import java.util.List;

import com.ftninformatika.jwd.test.model.Kategorija;


public interface KategorijaService {
	
	Kategorija findOneById(Long id);

	List<Kategorija> findAll();
	

}
