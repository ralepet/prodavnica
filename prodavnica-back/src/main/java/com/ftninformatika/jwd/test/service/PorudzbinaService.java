package com.ftninformatika.jwd.test.service;

import java.util.List;

import com.ftninformatika.jwd.test.model.Porudzbina;


public interface PorudzbinaService {
	
	List<Porudzbina> findAll();

	Porudzbina save(Porudzbina porudzbina);

}
