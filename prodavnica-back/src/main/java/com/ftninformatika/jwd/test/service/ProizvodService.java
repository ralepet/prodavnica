package com.ftninformatika.jwd.test.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.test.model.Proizvod;

public interface ProizvodService {
	
	Proizvod findOneById(Long id);

	Proizvod save(Proizvod proizvod);

	Proizvod update(Proizvod proizvod);

	Proizvod delete(Long id);

	Page<Proizvod> findAll(Integer pageNo);

	List<Proizvod> findAll();
	
	Proizvod oduzmiStanje(Proizvod proizvod, int brojNaruceni);
	
	Proizvod dodajStanje(Proizvod proizvod, int brojNarucenih);
	
	Page<Proizvod> find(Double cenaOd, Double cenaDo,Long kategorija_id, Integer pageNo);

}
