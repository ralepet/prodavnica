package com.ftninformatika.jwd.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Kategorija;
import com.ftninformatika.jwd.test.repository.KategorijaRepository;
import com.ftninformatika.jwd.test.service.KategorijaService;

@Service
public class JpaKategorijaService implements KategorijaService{
	
	@Autowired
	private KategorijaRepository kategorijaRepository;

	@Override
	public Kategorija findOneById(Long id) {
		return kategorijaRepository.findOneById(id);
	}

	@Override
	public List<Kategorija> findAll() {
		return kategorijaRepository.findAll();
	}

}
