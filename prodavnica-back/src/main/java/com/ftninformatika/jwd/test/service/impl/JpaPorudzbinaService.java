package com.ftninformatika.jwd.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Porudzbina;
import com.ftninformatika.jwd.test.repository.PorudzbinaRepository;
import com.ftninformatika.jwd.test.service.PorudzbinaService;

@Service
public class JpaPorudzbinaService implements PorudzbinaService{
	
	@Autowired
	private PorudzbinaRepository porudzbinaRepository;

	@Override
	public List<Porudzbina> findAll() {
		return porudzbinaRepository.findAll();
	}

	@Override
	public Porudzbina save(Porudzbina porudzbina) {
		return porudzbinaRepository.save(porudzbina);
	}

}
