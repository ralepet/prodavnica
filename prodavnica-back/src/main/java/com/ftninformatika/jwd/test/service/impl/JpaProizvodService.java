package com.ftninformatika.jwd.test.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Proizvod;
import com.ftninformatika.jwd.test.repository.ProizvodRepository;
import com.ftninformatika.jwd.test.service.ProizvodService;

@Service
public class JpaProizvodService implements ProizvodService{
	
	@Autowired
	private ProizvodRepository proizvodRepository;

	@Override
	public Proizvod findOneById(Long id) {
		return proizvodRepository.findOneById(id);
	}

	@Override
	public Proizvod save(Proizvod proizvod) {
		return proizvodRepository.save(proizvod);
	}

	@Override
	public Proizvod update(Proizvod proizvod) {
		return proizvodRepository.save(proizvod);
	}

	@Override
	public Proizvod delete(Long id) {
		Optional<Proizvod> proizvod = proizvodRepository.findById(id); 
		if (proizvod.isPresent()) {
			proizvodRepository.deleteById(id);
			return proizvod.get();
		}
		return null;
	}

	@Override
	public Page<Proizvod> findAll(Integer pageNo) {
		return proizvodRepository.findAll(PageRequest.of(pageNo, 4));
	}

	@Override
	public List<Proizvod> findAll() {
		return proizvodRepository.findAll();
	}

	@Override
	public Proizvod oduzmiStanje(Proizvod proizvod, int brojKupljenih) {
		proizvod.setStanje(proizvod.getStanje() - brojKupljenih);
		return proizvodRepository.save(proizvod);
	}
	
	@Override
	public Proizvod dodajStanje(Proizvod proizvod, int brojNarucenih) {
		proizvod.setStanje(proizvod.getStanje() + brojNarucenih);
		return proizvodRepository.save(proizvod);
	}

	@Override
	public Page<Proizvod> find(Double cenaOd, Double cenaDo,Long kategorija_id, Integer pageNo) {
		
		if(cenaOd == null) {
			cenaOd = 0.0;
		}
		if(cenaDo == null) {
			cenaDo = Double.MAX_VALUE;
		}
		
		if(kategorija_id != null) {
			return proizvodRepository.findByCenaBetweenAndKategorijaId(cenaOd, cenaDo, kategorija_id, PageRequest.of(pageNo, 3));
		}
		
		return proizvodRepository.findByCenaBetween(cenaOd, cenaDo, PageRequest.of(pageNo, 3));
	}

	

}
