package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.KategorijaDto;
import com.ftninformatika.jwd.test.model.Kategorija;

@Component
public class KategorijaToKategorijaDto implements Converter<Kategorija, KategorijaDto>{

	@Override
	public KategorijaDto convert(Kategorija source) {
		KategorijaDto dto = new KategorijaDto();
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		return dto;
	}
	
	public List<KategorijaDto> convert(List<Kategorija>list){
		List<KategorijaDto>dto = new ArrayList<KategorijaDto>();
		for(Kategorija kategorija : list) {  
			dto.add(convert(kategorija));
		}
		return dto;
	}
	

}
