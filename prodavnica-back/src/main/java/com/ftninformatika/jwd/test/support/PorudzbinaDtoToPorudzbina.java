package com.ftninformatika.jwd.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.PorudzbinaDto;
import com.ftninformatika.jwd.test.model.Porudzbina;
import com.ftninformatika.jwd.test.model.Proizvod;
import com.ftninformatika.jwd.test.service.ProizvodService;

@Component
public class PorudzbinaDtoToPorudzbina implements Converter<PorudzbinaDto, Porudzbina>{
	
	@Autowired
	private ProizvodService proizvodService;

	@Override
	public Porudzbina convert(PorudzbinaDto dto) {
		Porudzbina porudzbina = new Porudzbina();
		porudzbina.setId(dto.getId());
		porudzbina.setKolicina(dto.getKolicina());
		Proizvod proizvod = proizvodService.findOneById(dto.getIdProizvoda());
		porudzbina.setProizvod(proizvod);
		return porudzbina;
	}

}
