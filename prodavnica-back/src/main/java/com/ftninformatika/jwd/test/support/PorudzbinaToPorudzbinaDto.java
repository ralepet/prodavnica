package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.PorudzbinaDto;
import com.ftninformatika.jwd.test.model.Porudzbina;


@Component
public class PorudzbinaToPorudzbinaDto implements Converter<Porudzbina, PorudzbinaDto>{

	@Override
	public PorudzbinaDto convert(Porudzbina source) {
		PorudzbinaDto dto = new PorudzbinaDto();
		dto.setId(source.getId());
		dto.setIdProizvoda(source.getProizvod().getId());
		dto.setKolicina(source.getKolicina());
		dto.setNazivProizvoda(source.getProizvod().getNaziv());
		return dto;
	}
	
	public List<PorudzbinaDto> convert(List<Porudzbina>list){
		List<PorudzbinaDto>dto = new ArrayList<>();
		for(Porudzbina porudzbina : list) {     
			dto.add(convert(porudzbina));
		}
		return dto;
	}
	

}
