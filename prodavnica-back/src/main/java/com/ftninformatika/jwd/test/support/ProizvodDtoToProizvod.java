package com.ftninformatika.jwd.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.ProizvodDto;
import com.ftninformatika.jwd.test.model.Kategorija;
import com.ftninformatika.jwd.test.model.Proizvod;
import com.ftninformatika.jwd.test.service.KategorijaService;
import com.ftninformatika.jwd.test.service.ProizvodService;

@Component
public class ProizvodDtoToProizvod implements Converter<ProizvodDto, Proizvod>{
	
	@Autowired
	private ProizvodService proizvodService;
	
	@Autowired
	private KategorijaService kategorijaService;

	@Override
	public Proizvod convert(ProizvodDto dto) {
		Proizvod proizvod = null;
		

		if (dto.getId() != null) {
			proizvod = proizvodService.findOneById(dto.getId());
		}
		
		if(proizvod == null) {
			proizvod = new Proizvod();
        }
		
		proizvod.setCena(dto.getCena());
		proizvod.setStanje(dto.getStanje());
		proizvod.setNaziv(dto.getNaziv());
		
		Kategorija kategorija = kategorijaService.findOneById(dto.getIdKategorije());
		if( kategorija != null) {
			proizvod.setKategorija(kategorija);
		}
		
		return proizvod;
	}

}
