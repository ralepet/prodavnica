package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.ProizvodDto;
import com.ftninformatika.jwd.test.model.Proizvod;

@Component
public class ProizvodToProizvodDto implements Converter<Proizvod, ProizvodDto>{

	@Override
	public ProizvodDto convert(Proizvod source) {
		ProizvodDto dto = new ProizvodDto();
		dto.setId(source.getId());
		dto.setCena(source.getCena());
		
		if(source.getKategorija() != null) {
			dto.setIdKategorije(source.getKategorija().getId());
			dto.setNazivKategorije(source.getKategorija().getNaziv());
		}
		
		dto.setNaziv(source.getNaziv());
		dto.setStanje(source.getStanje());
		return dto;
	}
	
	public List<ProizvodDto> convert(List<Proizvod>list){
		List<ProizvodDto>dto = new ArrayList<>();
		for(Proizvod proizvod : list) {   
			dto.add(convert(proizvod)); 
		}
		return dto;
	}

}
