INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (4,'mare@gmail.com','mare','$2a$10$hW5o6Trv06hWFfqfm2zN9.foYgCIcxwVl7wGWUo0QMdiCLOsIpnk2','Marko','Markovic','ADMIN');
              
              
INSERT INTO kategorija (id, naziv) VALUES (1,'Periferije');
INSERT INTO kategorija (id, naziv) VALUES (2,'GPU');
INSERT INTO kategorija (id, naziv) VALUES (3,'Monitori');
INSERT INTO kategorija (id, naziv) VALUES (4,'CPU');

              
INSERT INTO proizvod (id, cena, naziv, stanje, kategorija_id) VALUES (1, 3000.0, 'LG mis', 12, 1);
INSERT INTO proizvod (id, cena, naziv, stanje, kategorija_id) VALUES (2, 133000.0, 'Nvidia GeForce 3060', 7, 2);
INSERT INTO proizvod (id, cena, naziv, stanje, kategorija_id) VALUES (3, 2500.0, 'LG monitor', 15, 3);
INSERT INTO proizvod (id, cena, naziv, stanje, kategorija_id) VALUES (4, 35000.0, 'AMD Ryzen', 12, 4);

INSERT INTO porudzbina (id, kolicina, proizvod_id) VALUES (1, 3, 1);
INSERT INTO porudzbina (id, kolicina, proizvod_id) VALUES (2, 2, 2);
INSERT INTO porudzbina (id, kolicina, proizvod_id) VALUES (3, 5, 3);
              
         



