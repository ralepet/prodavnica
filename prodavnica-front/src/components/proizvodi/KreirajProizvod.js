import React from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import TestAxios from '../../apis/TestAxios';
import { withNavigation } from "../../routeconf";

class KreirajProizvod extends React.Component {

    constructor(props){
        super(props);

        let proizvod = {
            cena: "",
            naziv: "" ,
            stanje: "", 
            idKategorije: "",
        }

        this.state = {
           
            proizvod: proizvod,
            kategorije: []
        }

    }

    componentDidMount(){
        this.getKategorije();
    }

    // TODO: Dobaviti filmove
    async getKategorije(){
        try{
            let result = await TestAxios.get("/kategorije");
            let kategorije = result.data;
            this.setState({kategorije: kategorije});
            console.log("uspesno su dobavljene kategorije");
            //alert("uspesno su dobavljene kategorije");
        }catch(error){
            console.log(error);
            alert("kategorije nisu uspesno dobavljene");
        }
    }

    async kreiraj(e){ 
        e.preventDefault();

        try{
            let proizvod = this.state.proizvod;
            console.log(proizvod)

            let proizvodDto = {
               
              naziv: proizvod.naziv,
              stanje: proizvod.stanje,
              cena: proizvod.cena,
              idKategorije: proizvod.idKategorije
            }
            await TestAxios.post("/proizvodi", proizvodDto);
            //let response = await TestAxios.post("/vina", vinoDto);
            this.props.navigate("/proizvodi");
        }catch(error){
            alert("Proizvod nije kreiran");
        }
    }

    valueInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let proizvod = this.state.proizvod;
        proizvod[name] = value;
    
        this.setState({ proizvod: proizvod });
      }
    

    render() {
        return (
          <>
            <Row>
              <Col></Col>
              <Col xs="12" sm="10" md="8">
                <h1>Dodaj novi proizvod</h1>
                <Form>
                  <Form.Label htmlFor="naziv">Naziv proizvoda</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Naziv proizvoda"
                    name="naziv"
                    onChange={(e) => this.valueInputChanged(e)}
                    />

                    <Form.Label htmlFor="cena">Cena</Form.Label>
                    <Form.Control
                        type="text"
                        name="cena"
                        placeholder="Cena proizvoda"
                        onChange={(e) => this.valueInputChanged(e)}
                     />
                    <Form.Label htmlFor="godinaProizvodnje">Stanje</Form.Label>
                    <Form.Control
                    type="text"
                    name="stanje"
                    placeholder="Stanje"
                    onChange={(e) => this.valueInputChanged(e)}
                     />
                    <Form.Label>Kategorija</Form.Label> 
                    <Form.Control as="select" name="idKategorije" onChange={event => this.valueInputChanged(event)}>
                        <option></option>
                        {
                            this.state.kategorije.map((kategorija) => {
                                return (
                                    <option key={kategorija.id} value={kategorija.id}>{kategorija.naziv}</option>
                                )
                            })
                        }
                    </Form.Control><br/> 
    
                  <Button style={{ marginTop: "25px" }} onClick={(event)=>{this.kreiraj(event);}}>
                    Kreiraj Proizvod
                  </Button>
                </Form>
              </Col>
              <Col></Col>
            </Row>                           
            
          </>
        );
      }
}

export default withNavigation(KreirajProizvod);