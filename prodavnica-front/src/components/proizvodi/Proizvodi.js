import React from 'react';
import TestAxios from '../../apis/TestAxios';
import { Row, Col, Button, Table, Form } from 'react-bootstrap'
import './../../index.css';
import { withParams, withNavigation } from '../../routeconf'

class Proizvodi extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            checked: false,
            proizvodi: [], 
            pretraga: { 
                cenaOd: "",
                cenaDo: "",
                kategorijaId: ""
            },
            currentPage:0, 
            totalPages:0,
            kategorije: [],
            brojZaPromenu :0
        }
    }

    componentDidMount() {
        this.getProizvodi(0);
        this.getKategorije();
    }

    // TODO:
    async getKategorije(){
        try{
            let result = await TestAxios.get("/kategorije");
            let kategorije = result.data; 
            this.setState({kategorije: kategorije});
            console.log("kategorije su uspesno dobavljene");
        }catch(error){
            console.log(error);
            alert("kategorije nisu dobavljene");
        } 
    }

    async getProizvodi(pageNo) {
        const config = {
            params: {
                pageNo: pageNo,
                cenaOd: this.state.pretraga.cenaOd,
                cenaDo: this.state.pretraga.cenaDo,
                kategorija_id: this.state.pretraga.kategorijaId
            }
        }
        try {
            let result = await TestAxios.get('/proizvodi', config);
            let total_pages = result.headers["total-pages"]
            this.setState({
                proizvodi: result.data,
              currentPage: pageNo,
              totalPages: total_pages
            });
          } catch (error) {
            console.log(error);
          }
    }

  
    async naruci(idProizvoda) {
        var params = {
            'kolicina': this.state.brojZaPromenu,
            'idProizvoda' : idProizvoda,
        };

        try {
            let result = await TestAxios.post('/porudzbine/naruci', params);
            console.log(result);
            //alert('proizvod je uspesno narucen!');
            window.location.reload();
          } catch (error) {
            // handle error
            console.log(error);
            alert('proizvod nije uspesno narucen!');
          }
    }

    
    async kupi(idProizvoda, brojDoStupnihProizvoda) {
        var params = {
            'kolicina': this.state.brojZaPromenu,
            'idProizvoda' : idProizvoda,
        };

        if(brojDoStupnihProizvoda >= this.state.brojZaPromenu){
            try {
                let result = await TestAxios.post('/porudzbine/', params);
                console.log(result);
                //alert('Proizvodi su uspesno kupljeni!');
                window.location.reload();
              } catch (error) {
                // handle error
                console.log(error);
                alert('Proizvodi nisu uspesno kupljeni!');
              }
        } else{
            alert('Ne mozete naruciti vise proizvoda nego sto ih ima na stanju!');
        }

    }
    

    idiNaKreiranje() {
        this.props.navigate('/proizvodi/kreiraj');
    }

    obrisi(proizvodId) {
        TestAxios.delete('/proizvodi/' + proizvodId)
            .then(res => {
                // handle success
                console.log(res);
                alert('Proizvod je uspesno obrisan!');
                window.location.reload();
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Error occured please try again!');
            });
    }

    idiNaPrethodnuStranicu(){
        this.getProizvodi(this.state.currentPage-1) 
    }

    idiNaSledecuStranicu(){ //
        this.getProizvodi(this.state.currentPage+1) 
    }

    onInputChange(event) {
        const name = event.target.name;
        const value = event.target.value

        let pretraga = this.state.pretraga;
        pretraga[name] = value;

        this.setState({ pretraga: pretraga })
    }

    brojProizvodaChange(event) {
        const value = event.target.value

        this.setState({ brojZaPromenu: value })
    }

    
    renderProizvoda() {
        return this.state.proizvodi.map((proizvod, index) => {
            return (
                <tr key={proizvod.id}>
                    <td>{proizvod.naziv}</td>
                    <td>{proizvod.cena}</td>
                    <td>{proizvod.stanje}</td>
                    <td>{proizvod.nazivKategorije}</td>
                    <td><Form.Control name="brojZaPromenu" min='0' type="number" onChange={(e) => this.brojProizvodaChange(e)}></Form.Control></td>
                    {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <td><Button variant="success" onClick={()=>this.naruci(proizvod.id)}>Nabavi</Button></td>
                         : null } 
                    {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <td><Button variant="danger" onClick={() => this.obrisi(proizvod.id)}>Obrisi</Button></td>
                        : <td></td>}
                    {window.localStorage.getItem("role") === "ROLE_KORISNIK" ?
                        <td><Button variant="success" onClick={()=>this.kupi(proizvod.id, proizvod.stanje)}>Kupi</Button></td>
                    : null}
                </tr>
            )
        })
    }


    render() {
        return (
            <Col>
                <Row><h1>Proizvod</h1></Row>
                <>
                    <Form.Check onChange={() => this.setState({ checked: !this.state.checked })} label="Sakrij pretragu" />
                </>
                <Form hidden={this.state.checked} style={{ width: "100%" }}>                    

                    <Form.Group>
                        <Form.Label>Cena od</Form.Label>
                        <Form.Control name="cenaOd" type="number" onChange={(e) => this.onInputChange(e)}></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Cena do</Form.Label>
                        <Form.Control name="cenaDo" type="number" onChange={(e) => this.onInputChange(e)}></Form.Control>
                    </Form.Group>

                    <Form.Label>Kategorija</Form.Label> 
                    <Form.Control as="select"  name="kategorijaId" onChange={e => this.onInputChange(e)}>
                        <option value="">Izaberi kategoriju</option>
                        {
                            this.state.kategorije.map((kategorija) => {
                                return (
                                    <option key={kategorija.id} value={kategorija.id}>{kategorija.naziv}</option>
                                )
                            })
                        }
                    </Form.Control><br/> 
                    <Button onClick={() => this.getProizvodi(0)}>Pretraga</Button>
                </Form>
                <br /><br />
                <Row>
                    {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <Button variant="primary" onClick={() => this.idiNaKreiranje()}>Dodajte proizvod</Button> : null
                    }
                    <br /><br />
                </Row>
                <Row>
                    <Table className="table table-striped" style={{ marginTop: 5 }} >
                        <thead className="thead-dark">
                            <tr>
                                <th>Naziv</th>
                                <th>Cena</th> 
                                <th>Stanje</th>
                                <th>Kategorija</th> 
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderProizvoda()}
                        </tbody>
                    </Table>
                </Row>
                <Row>
                <Button variant="primary" disabled={this.state.currentPage===0} onClick={()=>this.idiNaPrethodnuStranicu()}>Prethodna</Button>
                <Button variant="primary" disabled={this.state.currentPage===this.state.totalPages-1} onClick={()=>this.idiNaSledecuStranicu()}>Sledeca</Button>
                </Row>
            </Col>
        );
    }
}

export default withNavigation(withParams(Proizvodi));